package com.gms.asmakh.fragment.checkin

import android.content.Context
import android.util.Log
import com.gms.asmakh.base.BasePresenter
import com.gms.asmakh.model.api.checkregistration.building_login.BuildingLoginNetworkResponse
import com.gms.asmakh.model.api.checkregistration.building_login.InputBuildingLogin
import com.gms.asmakh.network.NetworkCallback
import com.gms.asmakh.utils.ObjectFactory.getInstance

class CheckInPresenter  (checkInView  : CheckInView?) : BasePresenter<CheckInView?>(){

    init {
        super.attachView(checkInView)
    }


    fun buildingLoginApi(context: Context?, buildingCode: String) {
//        view!!.showLoader()

        val inputBuildingLogin = InputBuildingLogin()
        inputBuildingLogin.code = buildingCode

        addSubscribe(apiStores.get_login(inputBuildingLogin), object : NetworkCallback<BuildingLoginNetworkResponse?>() {

            override fun onSuccess(model:  BuildingLoginNetworkResponse?) {

                view!!.hideLoader()

                if (model?.status.equals("success")) {
                    getInstance(context).appPreferenceManager.loginStatus = true
                    getInstance(context).appPreferenceManager.buildingId = Integer.parseInt(model?.details!!.buildingId!!)
                    getInstance(context).appPreferenceManager.buildingName = model?.details!!.buildingName
                    view!!.onCheckinSuccess()

                    Log.e("Tag_Test",  " buildingCode = " + buildingCode + " , " +
                        "model?.details!!.buildingId!! = " + model?.details!!.buildingId!!)

                }
                else{
                    view?.onError(model?.message)
                }
            }
            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })
    }

}