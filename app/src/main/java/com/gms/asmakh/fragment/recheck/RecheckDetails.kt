package com.gms.asmakh.fragment.recheck

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.hardware.Camera
import android.hardware.Camera.CameraInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.PermUtil
import com.gms.asmakh.R
import com.gms.asmakh.base.MvpFragment
import com.gms.asmakh.fragment.qrscanner.QRscannerFragment
import com.gms.asmakh.model.api.checkregistratn.CheckRegistrationNetworkResponse
import com.gms.asmakh.model.api.list_flat.FlatListNetworkResponse
import com.gms.asmakh.model.api.list_flat.FlatsItem
import com.gms.asmakh.model.api.purpose_visit.DetailsItem
import com.gms.asmakh.model.api.purpose_visit.PurposeNetworkResponse
import com.gms.asmakh.model.api.qrResult.Cte
import com.gms.asmakh.model.api.register.InputRegister
import com.gms.asmakh.model.api.register.QRDetail
import com.gms.asmakh.utils.ObjectFactory
import com.gms.asmakh.utils.Utils
import com.gms.guestmanagment.activity.details.DetailActivity
import kotlinx.android.synthetic.main.layout_loader.*
import kotlinx.android.synthetic.main.recheck_details.*
import kotlinx.android.synthetic.main.select_apartment_number.*
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*
import android.widget.TextView
import com.gms.asmakh.model.api.checkregistration.building_login.BuildingLoginNetworkResponse
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_message_dialog.view.*


class RecheckDetails : MvpFragment<RecheckDetailsPresenter?>(), RecheckDetailsView, ClickItems {

    private var rootView: View? = null
    lateinit var purposeAlertRecyclerAdapter: PurposeAlertRecyclerAdapter
    lateinit var sElectApartmentAlertRecyclerAdapter: SElectApartmentAlertRecyclerAdapter
    lateinit var mTxtPurpose: TextView
    lateinit var mTxtFlat: TextView

    private val CAMERA_REQUEST = 1888
    var captureimageUrl: String? = ""
    var selectimageUrl: String? = ""
    private val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1200
    private val REQUEST_CODE_INVOICE_IMAGE = 1000
    val REQ_CODE_STORAGE = 100
    private val CAPTURE_PHOTO = 4
    val REQUEST_IMAGE_CAPTURE = 200
    var mSelectedPurpseOfVisit: String? = null
    lateinit var recycleView_purposeofvisit: RecyclerView
    val CAMERA_BACK = "0"
    private var cameraId = CAMERA_BACK
    lateinit var mTxtFlatNum: String
    lateinit var mFlatId: String
    lateinit var mBuildingId: String
    var selectedImage: Uri? = null
    lateinit var mAlertDialog: AlertDialog

    var MobilePattern = "[0-9]{10}"
    var mSelectedPurpose: String? = null
    var mPurpose: String? = null

    private var barcodeData = ""
    var qr_data: Cte? = null

    var imageBitmap: Bitmap? = null
    var imgStatus = -1
    var imageUrl: String? = ""

    private var isScanned: Boolean = false
    private val number = ArrayList<Int>()

    lateinit var mCamera: Camera
    private val requestCodePicker = 100
    private lateinit var options: Options

    private var returnValue = ArrayList<String>()
    private var value = ArrayList<String>()
    private lateinit var myAdapter: MyAdapter

    private lateinit var builder: AlertDialog.Builder
    private lateinit var customLayout: View
    private lateinit var dialog: AlertDialog

    override fun createPresenter(): RecheckDetailsPresenter {
        return RecheckDetailsPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.recheck_details, container, false)
            .also { rootView = it }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        onClick()
    }

    private fun init() {
        recycleView_purposeofvisit = rootView!!.findViewById(R.id.recycleView_purposeofvisit)
        mTxtFlat = rootView!!.findViewById(R.id.txt_flat_num)

        presenter!!.getPurposeList()

        myAdapter = MyAdapter(requireContext())
        val sdfdate = SimpleDateFormat("MMM d, yyyy")
        val sdftime = SimpleDateFormat("hh:mm:ss a")

        val currentDateTimeString = sdfdate.format(Date())
        val currentTimeString = sdftime.format(Date())
        //val currentDateTimeString = DateFormat.getDateTimeInstance().format(Date())
        txt_date_time.text = currentDateTimeString
        txt_time.text = currentTimeString
        Log.d("DATE TIME", "init: "+currentDateTimeString)

        val number_1 = rootView!!.findViewById<Button>(R.id.num_1)
        val number_2 = rootView!!.findViewById<Button>(R.id.num_2)
        val number_3 = rootView!!.findViewById<Button>(R.id.num_3)
        val number_4 = rootView!!.findViewById<Button>(R.id.num_4)
        val number_5 = rootView!!.findViewById<Button>(R.id.num_5)
        val number_6 = rootView!!.findViewById<Button>(R.id.num_6)
        val number_7 = rootView!!.findViewById<Button>(R.id.num_7)
        val number_8 = rootView!!.findViewById<Button>(R.id.num_8)
        val number_9 = rootView!!.findViewById<Button>(R.id.num_9)
        val number_0 = rootView!!.findViewById<Button>(R.id.num_0)
        val buttonDelete = rootView!!.findViewById<Button>(R.id.img_dlt)

        number_1.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(1)
        })

        number_2.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(2)
        })

        number_3.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(3)
        })

        number_4.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(4)
        })

        number_5.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(5)
        })

        number_6.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(6)
        })

        number_7.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(7)
        })

        number_8.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(8)
        })

        number_9.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(9)
        })

        number_0.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(0)
        })

        buttonDelete.setOnClickListener(View.OnClickListener { v: View? -> deleteNumber() })
    }

    private fun onClick() {

        backArrowRecheck.setOnClickListener(View.OnClickListener {
            getActivity()?.onBackPressed()
        })

        img_takephoto.setOnClickListener(View.OnClickListener {
            showPickAlert(context)
        })

        qrscan.setOnClickListener(View.OnClickListener {
            startActivityForResuit(DetailActivity.Pages.QRSCANNER, QRscannerFragment.REQUESTCODE)
        })

        edt_mobile_number.setOnClickListener(View.OnClickListener {
//            showKeypadAlert()
        })

        logout.setOnClickListener {
            showLogoutAlert(context)
        }

        submit.setOnClickListener(View.OnClickListener {

            val inputRegister = InputRegister()

            /* if (selectedImage != null) {
                        val bitmap = MediaStore.Images.Media.getBitmap(context?.getContentResolver(), selectedImage)

                        //inputRegister.image = encodeTobase64(bitmap)
                       // inputRegister.image = encodeTobase64(imageBitmap!!)


                    } else {
                        inputRegister.image = ""
                    }*/

            if (imgStatus == -1) {
                inputRegister.image = ""
            } else {
                inputRegister.image = imageUrl
                inputRegister.image_stat = imgStatus
            }


            if (mSelectedPurpseOfVisit == null || mSelectedPurpseOfVisit!!.isEmpty()) {
                Toast.makeText(context, "Please Select Purpose Of Visit", Toast.LENGTH_SHORT).show()

            } else if (mTxtFlat.text.isEmpty()) {
                Toast.makeText(context, "Please Select Apartment Number", Toast.LENGTH_SHORT).show()

            } else if (edt_mobile_number.text.isEmpty()) {
                Toast.makeText(context, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show()

            } else if (edt_mobile_number!!.text.length < 13 || edt_mobile_number!!.text.length > 16) {

                Toast.makeText(context, "Invalid Mobile Number", Toast.LENGTH_SHORT).show()

            } else if (isScanned) {

                if (qr_data == null) {
                    Toast.makeText(
                            context,
                            "Invalid QR Code..Please Scan Your EHTERAZ",
                            Toast.LENGTH_SHORT
                    ).show()
                } else if (qr_data!!.v3!!.equals("#FFCA3C")) {
                    Toast.makeText(
                            context,
                            "Invalid QR Code..Please Scan Your EHTERAZ",
                            Toast.LENGTH_SHORT
                    ).show()
                } else if (qr_data!!.v3.equals("#FF0000")) {
                    Toast.makeText(
                            context,
                            "Invalid QR Code..Please Scan Your EHTERAZ",
                            Toast.LENGTH_SHORT
                    ).show()
                } else {

                    val qr_detail = QRDetail()

                    qr_detail.v1 = qr_data!!.v1.toString()
                    qr_detail.v2 = qr_data!!.v2.toString()
                    qr_detail.v3 = qr_data!!.v3.toString()
                    qr_detail.v4 = qr_data!!.v4.toString()
                    qr_detail.v5 = qr_data!!.v5.toString()
                    qr_detail.v6 = qr_data!!.v6.toString()
                    qr_detail.v7 = qr_data!!.v7.toString()

                    inputRegister.qr_detail = qr_detail

                    inputRegister.mobile = edt_mobile_number.text.toString()
                    inputRegister.name = txt_name.text.toString()

                    inputRegister.location = txt_Location.text.toString()
                    inputRegister.description = txt_name.text.toString()
                    inputRegister.purpose = mSelectedPurpseOfVisit
                    inputRegister.building_id = mBuildingId
                    inputRegister.flat_id = mFlatId
                    inputRegister.time = txt_date_time.text.toString()
                    inputRegister.device_id =
                            ObjectFactory.getInstance(context).appPreferenceManager.token
                    inputRegister.device_type = "android"

                    presenter?.userRegistration(context, inputRegister)

                    Log.d("input", "input$inputRegister")
                }
            } else if (!isScanned) {

                Toast.makeText(context, "Please Scan EHTERAZ", Toast.LENGTH_SHORT).show()
            }
        })

        layout_select_apartment_number.setOnClickListener(View.OnClickListener {
            showApartmentAlert(context)
        })


        edt_mobile_number.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER
                    || actionId == EditorInfo.IME_ACTION_DONE
            ) {

                presenter?.checkRegistration(context, edt_mobile_number.text.toString())

                true
            } else false
        })
    }

    private fun showLogoutAlert(context: Context?) {

//        val builder = AlertDialog.Builder(requireContext())
//        builder.setTitle(R.string.logout)
//        builder.setMessage("")
//        builder.setIcon(R.drawable.ic_logout)
//        builder.setCancelable(false)
//        builder.setPositiveButton(
//            R.string.Yes,
//            DialogInterface.OnClickListener { dialog, which ->
//                val intent = Intent(context, DetailActivity::class.java)
//                intent.putExtra("fragmentName", DetailActivity.Pages.RECHECK_DETAILS)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                startActivity(intent)
//            })
//        builder.setNegativeButton(
//            R.string.no,
//            DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
//        builder.create().show()


        try {
            builder = AlertDialog.Builder(context!!)
            customLayout = LayoutInflater.from(context).inflate(R.layout.custom_message_dialog,null)
            builder.setView(customLayout)
            dialog = builder.create()
            dialog.window?.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)

            customLayout.btn_msgs_ok.setOnClickListener {

                if (customLayout.edit_building_code.text.isEmpty()) {

                    customLayout.edit_building_code.error = "required"

                }else if (customLayout.edit_building_code.text == null)

                    customLayout.edit_building_code.error = "required"

                else {
                    presenter!!.buildingLoginApi(context, customLayout.edit_building_code.text.toString())
                }
            }

            customLayout.btn_msgs_cancel.setOnClickListener {

                dialog.dismiss()

//                val intent = Intent(context, DetailActivity::class.java)
//                intent.putExtra("fragmentName", DetailActivity.Pages.RECHECK_DETAILS)
//
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

//                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

//                startActivity(intent)
            }

            dialog.show()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showKeypadAlert() {

        val builder = AlertDialog.Builder(context!!)
        val view: View = LayoutInflater.from(context).inflate(R.layout.number, null)
        val number_1 = view.findViewById<CardView>(R.id.number_1)
        val number_2 = view.findViewById<CardView>(R.id.number_2)
        val number_3 = view.findViewById<CardView>(R.id.number_3)
        val number_4 = view.findViewById<CardView>(R.id.number_4)
        val number_5 = view.findViewById<CardView>(R.id.number_5)
        val number_6 = view.findViewById<CardView>(R.id.number_6)
        val number_7 = view.findViewById<CardView>(R.id.number_7)
        val number_8 = view.findViewById<CardView>(R.id.number_8)
        val number_9 = view.findViewById<CardView>(R.id.number_9)
        val number_0 = view.findViewById<CardView>(R.id.number_0)
        val buttonDelete = view.findViewById<CardView>(R.id.buttonDelete)
        val textView_mobileNumber = view.findViewById<TextView>(R.id.textView_mobileNumber)
        val LinearLayoutNext = view.findViewById<LinearLayout>(R.id.next)

        builder.setView(view)
        mAlertDialog = builder.create()


        number_1.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(1)
        })
        number_2.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(2)
        })
        number_3.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(3)
        })
        number_4.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(4)
        })
        number_5.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(5)
        })
        number_6.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(6)
        })
        number_7.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(7)
        })
        number_8.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(8)
        })
        number_9.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(9)
        })
        number_0.setOnClickListener(View.OnClickListener { v: View? ->
            setNumberText(0)
        })
        buttonDelete.setOnClickListener(View.OnClickListener { v: View? -> deleteNumber() })

        LinearLayoutNext.setOnClickListener(View.OnClickListener {
            edt_mobile_number.text = textView_mobileNumber.text.toString()
            mAlertDialog.dismiss()
        })

        mAlertDialog.show()

        mAlertDialog!!.getWindow()!!.setLayout(
                WindowManager.LayoutParams.FILL_PARENT,
                WindowManager.LayoutParams.FILL_PARENT
        )
    }

    private fun setNumberText(value: Int) {

        if (number.size < 11) {
            number.add(value)
        }
        setText()
    }

    private fun setText() {
        var stringNumber = ""
        for (i in number.indices) {
            stringNumber = stringNumber + number[i]
        }
        edt_mobile_number.setText("+974 " + stringNumber)

        ObjectFactory.getInstance(context).appPreferenceManager.mobile = edt_mobile_number.getText().toString()
    }

    private fun deleteNumber() {
        if (number.size > 0) {
            number.removeAt(number.size - 1)
            setText()
        }
    }

    private fun showApartmentAlert(context: Context?) {
        val builder = AlertDialog.Builder(context!!)
        val view: View = LayoutInflater.from(context).inflate(
                R.layout.select_apartment_number,
                null
        )

        builder.setView(view)
        mAlertDialog = builder.create()

        presenter!!.getFlatList(ObjectFactory.getInstance(context).appPreferenceManager.buildingId)

        mAlertDialog.show()
//        mAlertDialog.getWindow()!!.setLayout(600, 400);

        mAlertDialog!!.getWindow()!!.setLayout(
                WindowManager.LayoutParams.FILL_PARENT,
                WindowManager.LayoutParams.FILL_PARENT
        )

    }

    private fun showPurposeAlert(context: Context?) {
        val builder = AlertDialog.Builder(context!!)
        val view: View = LayoutInflater.from(context).inflate(R.layout.purpose_of_alert, null)

        builder.setView(view)
        mAlertDialog = builder.create()

        presenter!!.getPurposeList()

        mAlertDialog.show()

        mAlertDialog!!.getWindow()!!.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT
        )
    }

    private fun showPickAlert(context: Context?) {

        val builder = AlertDialog.Builder(context!!)
        val view: View = LayoutInflater.from(context).inflate(R.layout.alert_image_chooser, null)
        val ll_from_file = view.findViewById<LinearLayout>(R.id.ll_from_file)
        val ll_from_camera = view.findViewById<LinearLayout>(R.id.ll_from_camera)
        val imageView_close_alert = view.findViewById<ImageView>(R.id.imageView_close_alert)
        builder.setView(view)
        val alertDialog = builder.create()
        imageView_close_alert.setOnClickListener { alertDialog.dismiss() }

        ll_from_camera.setOnClickListener {

            if (hasCameraPermission()) {
                dispatchTakePictureIntent()
            }

            alertDialog.dismiss()
        }

        ll_from_file.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (hasStoragePermission(MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)) {
                    pickImageFromGallery()
                }
            } else {
                pickImageFromGallery()
            }
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    private fun hasStoragePermission(reqCode: Int): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(
                            getActivity()!!,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
            ) {
                return true
            }
            ActivityCompat.requestPermissions(
                    getActivity()!!,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQ_CODE_STORAGE
            )
            false
        } else {
            true
        }
    }

    private fun hasCameraPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(
                            context!!,
                            Manifest.permission.CAMERA
                    ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            context!!,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
            ) {
                return true
            }
            ActivityCompat.requestPermissions(
                    getActivity()!!,
                    arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_IMAGE_CAPTURE
            )
        } else {
            return true
        }
        return false
    }

    private fun pickImageFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(
                Intent.createChooser(intent, "SELECT IMAGE"),
                REQUEST_CODE_INVOICE_IMAGE
        )
    }

    private fun dispatchTakePictureIntent() {

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1)
        startActivityForResult(cameraIntent, CAMERA_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == QRscannerFragment.REQUESTCODE) {
            isScanned = true

            if (data != null) {
                barcodeData = data.getStringExtra("barCode")
                txt_qr.visibility = View.VISIBLE
                if (barcodeData.isEmpty() || barcodeData.equals("Invalid Qr")) {
                    txt_qr.text = "Invalid QR Code"
                    txt_qr.setTextColor(ContextCompat.getColor(context!!, R.color.wrong_qr))
                } else {
                    qr_data = data.getSerializableExtra("qrdata") as Cte;

                    if (qr_data != null) {
                        if (qr_data!!.v3.equals("#11855C")) {
                            txt_qr.text = qr_data!!.v4
                            txt_qr.setTextColor(ContextCompat.getColor(context!!, R.color.green_qr))
                        } else {
                            txt_qr.text = "Not allowed"
                            txt_qr.setTextColor(ContextCompat.getColor(context!!, R.color.wrong_qr))
                        }
                    }
                }

            }else{
                txt_qr.visibility = View.GONE
            }
        }

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {

                REQUEST_CODE_INVOICE_IMAGE -> if (data != null) {

                    selectedImage = data.data
                    Log.d("SELECT ", "onActivityResult: " + data)
                    val bitmap = MediaStore.Images.Media.getBitmap(context!!.getContentResolver(), selectedImage)


                    selectimageUrl = encodeTobase64(bitmap)
                    Glide.with(context!!).load(bitmap).into(profile_image)

                    Log.d("SELECT imageBitmap", "onActivityResult: " + bitmap)
                    Log.d("SELECT selectimageUrl", "onActivityResult: " + selectimageUrl)

                } else {
                    Toast.makeText(context, "dffff", Toast.LENGTH_LONG).show()
                }
                CAMERA_REQUEST -> try {

                    val extras = data!!.extras
                    imageBitmap = extras!!["data"] as Bitmap?

                    captureimageUrl = encodeTobase64(imageBitmap!!)
                    Glide.with(this).load(imageBitmap).into(profile_image)

                    Log.d("CAPTURE imageBitmap", "onActivityResult: " + imageBitmap)
                    Log.d("CAPTURE selectimageUrl", "onActivityResult: " + captureimageUrl)

                } catch (fe: NullPointerException) {
                    fe.printStackTrace()
                }
                else -> {
                }
            }
        }
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(
                inContext.contentResolver,
                inImage,
                "Title",
                null
        )
        return Uri.parse(path)

    }

    fun encodeTobase64(imageBitmap: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    override fun showLoader() {
        linearLayoutLoaderView.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        linearLayoutLoaderView.visibility = View.GONE

    }

    override fun onError(message: String?) {
        Utils.infoDialogue(context, message)
    }

    override fun onUserRegistrationSuccess(message: String?) {

        Log.e("VIEW", "ENTERED")
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("fragmentName", DetailActivity.Pages.SUCCESS)
        startActivity(intent)
        getActivity()?.finish()

    }

    override fun onCheckRegistrationSuccess(model: CheckRegistrationNetworkResponse) {

        edt_mobile_number.setText(model.userDetails?.mobile)
        person_name.setText(model.userDetails?.name)

        txt_Location.setText(model.userDetails?.location)
        txt_name.setText(model.userDetails?.name)
        Log.d("Name Check", "onClick: "+txt_name.text.toString())
//
        txt_flat_num.text = model.userDetails?.flatName


        if (model.userDetails?.image != null) {

            imageUrl = model.userDetails.image
            imgStatus = 0

            Glide.with(context!!).load(model.userDetails.image).centerCrop()
                    .placeholder(R.drawable.profile_img).into(profile_image)
        }
    }

    override fun onPurposeSuccess(model: PurposeNetworkResponse) {

        recycleView_purposeofvisit.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false))
        purposeAlertRecyclerAdapter = PurposeAlertRecyclerAdapter(this.context!!, model.details as List<DetailsItem>, this)
        recycleView_purposeofvisit?.setAdapter(purposeAlertRecyclerAdapter)
//        mAlertDialog.show()
    }

    override fun onFlatSuccess(model: FlatListNetworkResponse) {
        mAlertDialog.recyclerView_flat_number.setLayoutManager(GridLayoutManager(context, 3))

        sElectApartmentAlertRecyclerAdapter = SElectApartmentAlertRecyclerAdapter(this.context!!, model.flats as List<FlatsItem>, this)
        mAlertDialog.recyclerView_flat_number.setAdapter(sElectApartmentAlertRecyclerAdapter)

        mAlertDialog.show()
    }

    override fun RecheckDetailsSuccess(model: BuildingLoginNetworkResponse) {

        var build_id = model.details!!.buildingId
        var build_name = model.details!!.buildingName

        if (build_name == ObjectFactory.getInstance(context).appPreferenceManager.buildingName) {

                dialog.dismiss()

                ObjectFactory.getInstance(context).appPreferenceManager.loginStatus = false
                ObjectFactory.getInstance(context).appPreferenceManager.buildingName = ""

                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                startActivity(intent)
                getActivity()?.finish()
        }else {
            Toast.makeText(context, "Given building code has not matched!!", Toast.LENGTH_LONG).show()
        }
    }

    override fun clickedPurposeItem(purpose: String?) {
        mSelectedPurpseOfVisit = purpose!!
    }

    override fun clickedFlatNum(Number: String?, FlatId: String?, BuildingId: String?) {
        mFlatId = FlatId!!
        mBuildingId = BuildingId!!
        mTxtFlatNum = Number!!
        mTxtFlat.text = mTxtFlatNum
        mAlertDialog.dismiss()
    }

    fun startActivityForResuit(page: DetailActivity.Pages?, requestCode: Int) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("fragmentName", page)
        startActivityForResult(intent, requestCode)
    }

    fun QR_Empty_Data_Alert(context: Context, message: String) {
        val builder = AlertDialog.Builder(context!!)
        builder.setMessage(message)
        builder.setIcon(R.drawable.warning_icon)
        builder.setPositiveButton(R.string.yes, DialogInterface.OnClickListener { dialog, which ->

            dialog.dismiss()
        })
        builder.show()
    }

    private fun openFrontFacingCameraGingerbread(): Camera? {
        var cameraCount = 0
        var cam: Camera? = null
        val cameraInfo = Camera.CameraInfo()
        cameraCount = Camera.getNumberOfCameras()
        for (camIdx in 0 until cameraCount) {
            Camera.getCameraInfo(camIdx, cameraInfo)
            if (cameraInfo.facing === Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(camIdx)
                } catch (e: RuntimeException) {
                    Log.e("Your_TAG", "Camera failed to open: " + e.localizedMessage)
                }
            }
        }
        return cam
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Pix.start(this, options)
                } else {
                    Toast.makeText(context, "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    companion object {
        const val Fragment = "FRAGMENT"
    }

    private fun safeCameraOpen(id: Int): Boolean {
        return try {
            mCamera = Camera.open( CameraInfo.CAMERA_FACING_FRONT)
            true
        } catch (e: Exception) {
            Log.e(getString(R.string.app_name), "failed to open Camera")
            e.printStackTrace()
            false
        }
    }

}

//                inputRegister.mobile = edt_mobile_number.text.toString()
//                inputRegister.name = txt_name.text.toString()
//                inputRegister.location = txt_Location.text.toString()
//                inputRegister.description = txt_name.text.toString()
//                inputRegister.purpose = mSelectedPurpseOfVisit
//                inputRegister.building_id = mBuildingId
//                inputRegister.flat_id = mFlatId
//                inputRegister.time = txt_date_time.text.toString()
//                inputRegister.device_id =
//                        ObjectFactory.getInstance(context).appPreferenceManager.token
//                inputRegister.device_type = "android"
//
//                presenter?.userRegistration(context, inputRegister)
//
//                Log.d("input", "input$inputRegister")