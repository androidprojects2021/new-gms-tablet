package com.gms.asmakh.model.api.list_flat

import com.google.gson.annotations.SerializedName

data class FlatListNetworkResponse(

	@field:SerializedName("flats")
	val flats: List<FlatsItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class FlatsItem(

	@field:SerializedName("building_id")
	val buildingId: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("device_id")
	val deviceId: String? = null,

	@field:SerializedName("flat_id")
	val flatId: String? = null,

	@field:SerializedName("device_type")
	val deviceType: String? = null,

	@field:SerializedName("flat_num")
	val flatNum: String? = null,

	@field:SerializedName("username")
	val username: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
