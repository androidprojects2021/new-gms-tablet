package com.gms.asmakh.model.api.register

import com.google.gson.annotations.SerializedName

class InputRegister {
    @SerializedName("mobile")
    var mobile: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("description")
    var description: String? = null


    @SerializedName("flat_id")
    var flat_id: String? = null


    @SerializedName("location")
    var location: String? = null

    @SerializedName("purpose")
    var purpose: String? = null

    @SerializedName("image")
    var image: String? = null

    @SerializedName("time")
    var time: String? = null

    @SerializedName("device_type")
    var device_type: String? = null

    @SerializedName("device_id")
    var device_id: String? = null

    @SerializedName("image_stat")
    var image_stat: Int? = null

    @SerializedName("building_id")
    var building_id: String? = null

    @field:SerializedName("qr_detail")
    var qr_detail: QRDetail? = null




}
class QRDetail {

    @SerializedName("v4")
    var v4: String? = null

    @SerializedName("v2")
    var v2: String? = null

    @SerializedName("v6")
    var v6: String? = null

    @SerializedName("v9")
    var v9: String? = null

    @SerializedName("v1")
    var v1: String? = null

    @SerializedName("v7")
    var v7: String? = null

    @SerializedName("DS")
    var DS: String? = null

    @SerializedName("v5")
    var v5: String? = null

    @SerializedName("v8")
    var v8: String? = null

    @SerializedName("v3")
    var v3: String? = null

}