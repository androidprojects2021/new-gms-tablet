package com.gms.asmakh.fragment.recheck

import android.content.Context
import android.util.Log
import com.gms.asmakh.base.BasePresenter
import com.gms.asmakh.model.api.checkregistration.building_login.BuildingLoginNetworkResponse
import com.gms.asmakh.model.api.checkregistration.building_login.InputBuildingLogin
import com.gms.asmakh.model.api.checkregistratn.CheckRegistrationNetworkResponse
import com.gms.asmakh.model.api.checkregistratn.InputCheckRegistration
import com.gms.asmakh.model.api.list_flat.FlatListNetworkResponse
import com.gms.asmakh.model.api.list_flat.ListFlatInput
import com.gms.asmakh.model.api.purpose_visit.PurposeNetworkResponse
import com.gms.asmakh.model.api.register.InputRegister
import com.gms.asmakh.model.api.register.RegisterNetworkResponse
import com.gms.asmakh.network.NetworkCallback
import com.gms.asmakh.utils.ObjectFactory
import com.google.gson.Gson


class RecheckDetailsPresenter(recheckDetailsView: RecheckDetailsView?) : BasePresenter<RecheckDetailsView?>() {

    init {
        super.attachView(recheckDetailsView)
    }

    fun userRegistration(context: Context?, inputRegister: InputRegister) {
        view!!.showLoader()

        Log.d("jsondata", Gson().toJson(inputRegister))

        addSubscribe(
                apiStores.user_register(inputRegister),
                object : NetworkCallback<RegisterNetworkResponse?>() {
                    override fun onSuccess(model: RegisterNetworkResponse?) {
                        Log.e("RegisterNetworkResponse", "ENTERD")
                        view!!.hideLoader()
                        if (model?.status.equals("success")) {

                            Log.e("SUCCESS", "ENTERD")

                            view!!.onUserRegistrationSuccess(model!!.message)
                        } else {
                            Log.e("ERROR", "ENTERD")
                            view?.onError(model?.message)
                        }
                    }

                    override fun onFailure(message: String?) {

                        view!!.hideLoader()
                        view?.onError(message)

                    }

                    override fun onFinish() {

                    }
                })

    }

    fun checkRegistration(context: Context?, mobile: String) {

        val inputCheckRegistration = InputCheckRegistration()
        inputCheckRegistration.mobile = mobile

        addSubscribe(
                apiStores.check_registration(inputCheckRegistration),
                object : NetworkCallback<CheckRegistrationNetworkResponse?>() {
                    override fun onSuccess(model: CheckRegistrationNetworkResponse?) {
                        view!!.hideLoader()
                        if (model?.status.equals("success")) {
                            view!!.onCheckRegistrationSuccess(model!!)
                        } else {
//                        view?.onError(model?.message)
                        }
                    }

                    override fun onFailure(message: String?) {
                        view!!.hideLoader()
//                    view?.onError(message)
                    }

                    override fun onFinish() {

                    }
                })

    }

    fun getPurposeList() {
        view!!.showLoader()

        addSubscribe(
                apiStores.purpose_of_visit(),
                object : NetworkCallback<PurposeNetworkResponse?>() {
                    override fun onSuccess(model: PurposeNetworkResponse?) {
                        view!!.hideLoader()
                        if (model?.status.equals("success")) {
                            view!!.onPurposeSuccess(model!!)
                        }
                    }

                    override fun onFailure(message: String?) {
                        view!!.hideLoader()
                        view?.onError(message)
                    }

                    override fun onFinish() {

                    }
                })
    }

//    fun getFlatList() {
//        addSubscribe(apiStores.list_flat_building(listFlatInput), object : NetworkCallback<FlatListNetworkResponse?>() {
//            override fun onSuccess(model: FlatListNetworkResponse?) {
//                view!!.hideLoader()
//                if (model?.status.equals("success")) {
//                    view!!.onFlatSuccess(model!!)
//                }
//            }
//
//            override fun onFailure(message: String?) {
//                view!!.hideLoader()
//                view?.onError(message)
//            }
//
//            override fun onFinish() {
//
//            }
//        })
//    }

    fun userRegistration(
            context: Context?,
            mMobileNumber: String,
            mLocation: String,
            mName: String,
            mPurpose: String,
            mFlatNum: String) {

        view!!.showLoader()


        val inputRegister = InputRegister()

        inputRegister.mobile = mMobileNumber
        inputRegister.name = mName
        inputRegister.purpose= mPurpose
        inputRegister.building_id =mFlatNum

        addSubscribe(
                apiStores.user_register(inputRegister),
                object : NetworkCallback<RegisterNetworkResponse?>() {
                    override fun onSuccess(model: RegisterNetworkResponse?) {
                        view!!.hideLoader()
                        if (model?.status.equals("success")) {
                            view!!.onUserRegistrationSuccess(model!!.message)
                        } else {
                            view?.onError(model?.message)
                        }
                    }

                    override fun onFailure(message: String?) {
                        view!!.hideLoader()

                        view?.onError(message)


                    }

                    override fun onFinish() {

                    }
                })
    }

    fun getFlatList(buildingId: Int) {

        view!!.showLoader()

        val listFlatInput = ListFlatInput()
        listFlatInput.building_id = buildingId.toString()


        addSubscribe(apiStores.list_flat_building(listFlatInput), object : NetworkCallback<FlatListNetworkResponse?>() {
            override fun onSuccess(model: FlatListNetworkResponse?) {
                view!!.hideLoader()
                if (model?.status.equals("success")) {
                    view!!.onFlatSuccess(model!!)
                }
            }

            override fun onFailure(message: String?) {
                view!!.hideLoader()
                view?.onError(message)
            }

            override fun onFinish() {

            }
        })
    }


    fun buildingLoginApi(context: Context?, buildingCode: String) {
//        view!!.showLoader()

        val inputBuildingLogin = InputBuildingLogin()
        inputBuildingLogin.code = buildingCode

        addSubscribe(apiStores.get_login(inputBuildingLogin), object : NetworkCallback<BuildingLoginNetworkResponse?>() {

            override fun onSuccess(model:  BuildingLoginNetworkResponse?) {

                view!!.hideLoader()

                if (model?.status.equals("success")) {
                    ObjectFactory.getInstance(context).appPreferenceManager.loginStatus = true
                    ObjectFactory.getInstance(context).appPreferenceManager.buildingId = Integer.parseInt(model?.details!!.buildingId!!)
                    ObjectFactory.getInstance(context).appPreferenceManager.buildingName = model?.details!!.buildingName

                    view!!.RecheckDetailsSuccess(model!!)

                    Log.e("Tag_Test",  " buildingCode = " + buildingCode + " , " +
                            "model?.details!!.buildingId!! = " + model?.details!!.buildingId!!)

                }
                else{
                    view?.onError(model?.message)
                }
            }
            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })
    }

}