package com.gms.asmakh.model.api.register

import com.google.gson.annotations.SerializedName

data class RegisterNetworkResponse(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("responce_code")
	val responceCode: Int? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("register_id")
	val registerId: Int? = null
)
