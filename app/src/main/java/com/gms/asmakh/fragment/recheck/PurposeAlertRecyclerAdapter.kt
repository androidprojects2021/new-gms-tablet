package com.gms.asmakh.fragment.recheck

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.gms.asmakh.R
import com.gms.asmakh.model.api.purpose_visit.DetailsItem
import kotlinx.android.synthetic.main.custom_purpose_visit_view.view.*

class PurposeAlertRecyclerAdapter(val context: Context, val detailsItem: List<DetailsItem> = ArrayList(), val clickPurposeItems: ClickItems) : RecyclerView.Adapter<PurposeAlertRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PurposeAlertRecyclerAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_purpose_visit_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return detailsItem.size
    }

    override fun onBindViewHolder(holder: PurposeAlertRecyclerAdapter.ViewHolder, position: Int) {
        holder.itemView.textView_purpose.text = detailsItem[position].purpose



        if (detailsItem.get(position).selected) {
            holder.textviewpurpose!!.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.cardview.setBackground(context.getResources().getDrawable( R.drawable.bg_button))  //R.color.colorPrimary


        } else {
            holder.textviewpurpose!!.setTextColor(ContextCompat.getColor(context, R.color.text_black)) //R.color.colorPrimary
//            holder.cardview.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
           holder.cardview.setBackground(context.getResources().getDrawable(R.drawable.bg_square_blue)) //rectangle_shape_meroonborder


        }
//        if (detailsItem.get(position).selected) {
//            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shape_meroonborder))
//
//        }


        holder.itemView.setOnClickListener { v: View? ->
            for (i in detailsItem.indices) {
                detailsItem.get(i).selected = false
            }
            detailsItem.get(position).selected = true
            notifyDataSetChanged()
            clickPurposeItems.clickedPurposeItem(detailsItem[position].purpose)
        }





//
//
//        holder.itemView.setOnClickListener(View.OnClickListener {
//            clickPurposeItems.clickedPurposeItem(detailsItem[position].purpose)
//            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shape_meroonborder))
//        })

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

     val   textviewpurpose = itemView.findViewById<TextView?>(R.id.textView_purpose)
       val  cardview = itemView.findViewById<CardView>(R.id.cardview)
    }


}