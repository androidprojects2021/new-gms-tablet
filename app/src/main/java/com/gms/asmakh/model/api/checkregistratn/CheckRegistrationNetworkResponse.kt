package com.gms.asmakh.model.api.checkregistratn

import com.google.gson.annotations.SerializedName

data class CheckRegistrationNetworkResponse(

	@field:SerializedName("user_details")
	val userDetails: UserDetails? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("responce_code")
	val responceCode: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class UserDetails(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("purpose")
	val purpose: String? = null,

	@field:SerializedName("flat_id")
	val flatId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("create_date")
	val createDate: String? = null,

	@field:SerializedName("flat_name")
	val flatName: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
