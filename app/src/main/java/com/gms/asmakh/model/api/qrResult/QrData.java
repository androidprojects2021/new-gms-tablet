package com.gms.asmakh.model.api.qrResult;

import com.google.gson.annotations.SerializedName;

public class QrData{

	@SerializedName("imageXML")
	private ImageXML imageXML;

	public ImageXML getImageXML(){
		return imageXML;
	}
}