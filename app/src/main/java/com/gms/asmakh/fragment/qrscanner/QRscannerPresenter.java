package com.gms.asmakh.fragment.qrscanner;


import com.gms.asmakh.base.BasePresenter;

/**
 * Created by BlessonJRaj on 11/3/2019.
 */
public class QRscannerPresenter extends BasePresenter<QRscannerView> {
    public QRscannerPresenter(QRscannerView cartView) {
        super.attachView(cartView);
    }
}
