package com.gms.guestmanagment.activity.details

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.gms.asmakh.R
import com.gms.asmakh.base.MvpActivity
import com.gms.asmakh.fragment.checkin.CheckInFragment
import com.gms.asmakh.fragment.qrscanner.QRscannerFragment
import com.gms.asmakh.fragment.recheck.RecheckDetails
import com.gms.asmakh.fragment.success_fragment.SuccessFragment
import com.gms.asmakh.model.api.fcm_payload.FcmPayout
import com.gms.asmakh.model.api.fcm_payload.Userdetail
import com.gms.asmakh.utils.ActivityUtils
import com.google.gson.Gson

class DetailActivity : MvpActivity<DetailActivityPresenter?>(), DetailActivityView {

    var isFromNotification: Boolean = false
    lateinit var mAlertDialog: AlertDialog
    lateinit var timer:CountDownTimer
    enum class Pages {
        RECHECK_DETAILS,
        SUCCESS,
        CHECK_IN,
        QRSCANNER
    }

    override fun createPresenter(): DetailActivityPresenter {
        return DetailActivityPresenter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        supportActionBar?.hide();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                onBroadcastReceiver,
                IntentFilter("com.gms.asmakh")
        )

        if (intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!!.getString(key)
                if (key.equals("payload")) {
                    isFromNotification = true
                    val firebasePayloadData = Gson().fromJson(value, FcmPayout::class.java)
                    showAcceptAlert(this)
                }
                Log.d("DetailActivity", "Key: $key Value: $value")
            }
        }

        var page = Pages.RECHECK_DETAILS

        if (intent != null) {
            if (!isFromNotification) {
                page = intent.getSerializableExtra("fragmentName") as Pages
            }
        }

        loadPage(page)

    }

    private fun loadPage(pages: Pages) {
        when (pages) {
            Pages.RECHECK_DETAILS -> {
                val recheckDetails: Fragment = RecheckDetails()
                ActivityUtils.replaceFragment(
                        supportFragmentManager,
                        recheckDetails,
                        R.id.detailsactivity_content
                )
            }

            Pages.SUCCESS -> {
                val successFragment: Fragment = SuccessFragment()
                ActivityUtils.replaceFragment(
                        supportFragmentManager,
                        successFragment,
                        R.id.detailsactivity_content
                )
            }

            Pages.CHECK_IN -> {
                val checkInFragment: Fragment = CheckInFragment()
                ActivityUtils.replaceFragment(
                        supportFragmentManager,
                        checkInFragment,
                        R.id.detailsactivity_content
                )
            }
            Pages.QRSCANNER -> {
                val qRscannerFragment: Fragment = QRscannerFragment()
                ActivityUtils.replaceFragment(
                        supportFragmentManager,
                        qRscannerFragment,
                        R.id.detailsactivity_content
                )
            }

        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            // handling fragment backbutton navigation
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    private val onBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val firebasePayloadData = intent.getSerializableExtra("data") as Userdetail
//            showAcceptAlert(firebasePayloadData.acceptedStatus)

            if (firebasePayloadData.acceptedStatus.equals("2")) {

                        ShowRejectAlert(context)

            } else if (firebasePayloadData.acceptedStatus.equals("1")) {
                presenter!!.acceptedStatusDoorOpen(context)

                showAcceptAlert(context)


            }


        }
    }


    private fun ShowRejectAlert(context: Context) {

        val builder = AlertDialog.Builder(this)
        val viewGroup: ViewGroup = findViewById(android.R.id.content)
        val dialogView = LayoutInflater.from(this).inflate(R.layout.reject_layout, viewGroup, false)
        builder.setView(dialogView)
        val back_home: Button = viewGroup.findViewById(R.id.back_home)
        val img_back_error: ImageView = viewGroup.findViewById(R.id.img_back_error)
        mAlertDialog = builder.create()
        mAlertDialog.show()
        back_home.setOnClickListener {
            mAlertDialog.dismiss()
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
        img_back_error.setOnClickListener {
            mAlertDialog.dismiss()
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

       /* timer = object : CountDownTimer(4000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                mAlertDialog.dismiss()
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
        timer.start()*/

    }

    private fun showAcceptAlert(context: Context) {

        val builder = AlertDialog.Builder(this)
        val viewGroup: ViewGroup = findViewById(android.R.id.content)
        val dialogView: View = LayoutInflater.from(this).inflate(R.layout.custome_dialogue, viewGroup, false)
        builder.setView(dialogView)
        val back_home_success: Button = viewGroup.findViewById(R.id.back_home_success)
        val img_back_success: ImageView = viewGroup.findViewById(R.id.img_back_success)
        mAlertDialog = builder.create()

        mAlertDialog.setCanceledOnTouchOutside(true);

        mAlertDialog.show()
        back_home_success.setOnClickListener {
            mAlertDialog.dismiss()
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
        img_back_success.setOnClickListener {
            mAlertDialog.dismiss()
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

       /* timer = object : CountDownTimer(4000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
//                if (mAlertDialog.isShowing){
//                    mAlertDialog.dismiss()
//                }
//                if (context is Activity) {
//                    if (!context.isFinishing) mAlertDialog.dismiss()
//                }
                mAlertDialog.dismiss()
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
        timer.start()*/
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(onBroadcastReceiver)
        val builder = AlertDialog.Builder(this)
        mAlertDialog=builder.create()
        if (mAlertDialog!=null && mAlertDialog.isShowing){
            mAlertDialog.show()
        }else{
            mAlertDialog.dismiss()
        }

        super.onDestroy()
    }

    override fun onPause() {
        super.onPause()

//        onBroadcastReceiver.abortBroadcast

        val builder = AlertDialog.Builder(this)

        mAlertDialog=builder.create()
        if (mAlertDialog!=null && mAlertDialog.isShowing){
            mAlertDialog.show()
        }else{
            mAlertDialog.dismiss()
        }


    }


}

