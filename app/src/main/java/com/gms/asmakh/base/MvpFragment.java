package com.gms.asmakh.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.gms.asmakh.utils.ObjectFactory;
import java.util.Locale;
import java.util.Objects;


public abstract class MvpFragment<P extends BasePresenter> extends BaseFragment {

    protected P presenter;

    protected abstract P createPresenter();

    public long lastNetworkErrorShownTime = 0;

    private BroadcastReceiver broadcastReceiver = null;
    private IntentFilter intentFilter = null;

    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";

    // Whether there is a Wi-Fi connection.
    private static boolean wifiConnected = false;
    // Whether there is a mobile connection.
    private static boolean mobileConnected = false;
    // Whether the display should be refreshed.
    public static boolean refreshDisplay = true;

    // The user's current network preference setting.
    public static String sPref = null;

    // The BroadcastReceiver that tracks network connectivity changes.
//    private NetworkReceiver receiver = new NetworkReceiver();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        presenter = createPresenter();
        super.onViewCreated(view, savedInstanceState);
//        checkLanguage();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.dettachView();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.dettachView();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceivers();
        checkLanguage();
    }

    @Override
    public void onPause() {
        unregisterReceivers();
        super.onPause();
    }

    public void registerReceivers() {
        if (broadcastReceiver != null && intentFilter != null && getActivity() != null) {
            LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    public void unregisterReceivers() {
        if (broadcastReceiver != null && intentFilter != null && getActivity() != null) {
            LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(broadcastReceiver);
        }
    }

    public void addBroadcastListener(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        this.broadcastReceiver = broadcastReceiver;
        this.intentFilter = intentFilter;
    }
    public void checkLanguage() {

        String lang = ObjectFactory.getInstance(getContext()).getAppPreferenceManager().getLanguage();
        if (lang.equalsIgnoreCase(""))
            return;
        Locale  myLocale = new Locale(lang);//Set Selected Locale
        Locale.setDefault(myLocale);//set new locale as default
        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        Objects.requireNonNull(getActivity()).getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());//Update the config
//        updateText();//Update texts according to locale

    }


//    public static long showNetworkError(final Context context, View rootView, long lastNetworkErrorShownTime) {
//
//        if (rootView != null) {
//            long currentTimeMillis = System.currentTimeMillis();
//            if ((currentTimeMillis - lastNetworkErrorShownTime) > 5000) {
//                Snackbar snackbar = Snackbar.make(rootView, "No active network connection", Snackbar.LENGTH_INDEFINITE);
//                snackbar.setAction(R.string.settings, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
//                        context.startActivity(intent);
//                    }
//                });
//                snackbar.show();
//                return currentTimeMillis;
//            }
//        }
//        return 0;
//    }


    public boolean isConnected(){
        boolean connected = false;
        try{

            ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return  connected;
        }catch(Exception e){

        }
        return connected;
    }

}
