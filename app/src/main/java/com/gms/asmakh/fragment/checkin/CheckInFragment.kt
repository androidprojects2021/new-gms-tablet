package com.gms.asmakh.fragment.checkin

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gms.guestmanagment.activity.details.DetailActivity
import com.gms.asmakh.R
import com.gms.asmakh.base.MvpFragment
import com.gms.asmakh.utils.ObjectFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_loader.*

class CheckInFragment : MvpFragment<CheckInPresenter?>(), CheckInView {
    private var rootView: View? = null

    override fun createPresenter(): CheckInPresenter {
        return CheckInPresenter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_main, container, false).also { rootView = it }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        onClick()
    }

    private fun init() {
        if (ObjectFactory.getInstance(context).appPreferenceManager.loginStatus) {

            edt_building.setText(ObjectFactory.getInstance(context).appPreferenceManager.buildingName)
        }
    }

    private fun onClick() {

        check_in.setOnClickListener {

            if (ObjectFactory.getInstance(context).appPreferenceManager.loginStatus) {

                 onCheckinSuccess()

                presenter!!.buildingLoginApi(context, edt_building.text.toString())

            } else {
                if (edt_building.text.isEmpty()) {

                    edt_building.error = "required"


                }else if (edt_building.text == null)

                    edt_building.error = "required"

                else {
                    presenter!!.buildingLoginApi(context, edt_building.text.toString())
                }
            }
        }
    }

    override fun showLoader() {
        linearLayoutLoaderView.visibility = View.VISIBLE

    }

    override fun hideLoader() {
        linearLayoutLoaderView.visibility = View.GONE

    }

    override fun onError(message: String?) {
    }

    override fun onCheckinSuccess() {

        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("fragmentName", DetailActivity.Pages.RECHECK_DETAILS)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()

        if (ObjectFactory.getInstance(context).appPreferenceManager.loginStatus) {

            edt_building.setText(ObjectFactory.getInstance(context).appPreferenceManager.buildingName)
        }
    }

}