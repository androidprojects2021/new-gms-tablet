package com.gms.asmakh.model.api.checkregistration.building_login

import com.google.gson.annotations.SerializedName

data class BuildingLoginNetworkResponse(

	@field:SerializedName("details")
	val details: Details? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Details(

	@field:SerializedName("building_id")
	val buildingId: String? = null,

	@field:SerializedName("building_name")
	val buildingName: String? = null,

	@field:SerializedName("code")
	val code: String? = null
)
