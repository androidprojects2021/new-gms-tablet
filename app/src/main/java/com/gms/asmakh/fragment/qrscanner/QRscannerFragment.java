package com.gms.asmakh.fragment.qrscanner;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import com.gms.asmakh.R;
import com.gms.asmakh.base.MvpFragment;
import com.gms.asmakh.model.api.qrResult.QrData;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.IOException;
import java.util.Objects;

import static com.google.android.gms.vision.CameraSource.CAMERA_FACING_BACK;
import static com.google.android.gms.vision.CameraSource.CAMERA_FACING_FRONT;

public class QRscannerFragment extends MvpFragment<QRscannerPresenter> implements QRscannerView {

    private SurfaceView surfaceView;
    private TextView textViewBarCodeValue;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    private String intentData = "";
    public static int REQUESTCODE = 111;

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return rootView = inflater.inflate(R.layout.fragment_qr_scanner, container, false);
    }


    @Override
    protected QRscannerPresenter createPresenter() {
        return new QRscannerPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents();

    }

    @Override
    public void onPause() {
        super.onPause();
        cameraSource.release();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialiseDetectorsAndSources();
    }

    private void initComponents() {

        textViewBarCodeValue = rootView.findViewById(R.id.txtBarcodeValue);
        surfaceView = rootView.findViewById(R.id.surfaceView);
    }

    private void initialiseDetectorsAndSources() {
//        Toast.makeText(getContext(), "Barcode scanner started", Toast.LENGTH_SHORT).show();
        barcodeDetector = new BarcodeDetector.Builder(getContext())
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        cameraSource = new CameraSource.Builder(Objects.requireNonNull(getContext()), barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true) //you should add this feature
                .setFacing(CAMERA_FACING_FRONT)

                .build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                openCamera();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
//                Toast.makeText(getContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barCode = detections.getDetectedItems();
                if (barCode.size() > 0) {
                    setBarCode(barCode);
                }
            }
        });
    }

    private void openCamera() {
        try {
            if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                cameraSource.start(surfaceView.getHolder());
            } else {
                ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new
                        String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setBarCode(final SparseArray<Barcode> barCode) {
        textViewBarCodeValue.post(new Runnable() {
            @Override
            public void run() {
                intentData = barCode.valueAt(0).displayValue;
                textViewBarCodeValue.setText(intentData);
                QRscannerFragment.this.copyToClipBoard(intentData);
            }
        });
    }

    private void copyToClipBoard(String text) {
        JSONObject jsonObj = null;
        try {
            jsonObj = XML.toJSONObject(text);
            QrData data = new Gson().fromJson(jsonObj.toString(), QrData.class);
            if (data != null && data.getImageXML() != null && data.getImageXML().getCte() != null) {
                if (!data.getImageXML().getCte().getV3().isEmpty()) {
                    Intent intent = new Intent();
                    intent.putExtra("barCode", data.getImageXML().getCte().getV3());
                    intent.putExtra("qrdata", data.getImageXML().getCte());
                    Objects.requireNonNull(getActivity()).setResult(QRscannerFragment.REQUESTCODE, intent);
                    Objects.requireNonNull(getActivity()).onBackPressed();
                }
            }else {
                Toast.makeText(getContext(), "Invalid Qr", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.putExtra("barCode","Invalid Qr" );
                Objects.requireNonNull(getActivity()).setResult(QRscannerFragment.REQUESTCODE, intent);


                Objects.requireNonNull(getActivity()).onBackPressed();

            }
        } catch (JSONException e) {
            Log.e("JSON exception", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA_PERMISSION && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Objects.requireNonNull(getActivity()).finish();
            else
                openCamera();
        } else
            Objects.requireNonNull(getActivity()).finish();
    }
//    private void  xmlparse(String data){
//
//
//
//        XmlPullParserFactory pullParserFactory;
//
//        try {
//            pullParserFactory = XmlPullParserFactory.newInstance();
//            XmlPullParser parser = pullParserFactory.newPullParser();
//
//            InputStream in_s = getContext().getApplicationContext().getAssets().open("sample.xml");
//            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
//            parser.setInput(data, null);
//
////            ArrayList<Country> countries=  parseXML(parser);
////
////            String text="";
////
////            for(Country country:countries)
////            {
////
////                text+= "id : "+country.getId()+" name : "+country.getName()+" capital : "+country.getCapital()+"\n";
////            }
////
////            textView.setText(text);
//
//
//
//        } catch (XmlPullParserException e) {
//
//            e.printStackTrace();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//    }

//    public class StackOverflowXmlParser {
//        // We don't use namespaces
//        private static final String ns = null;
//
//        public List parse(InputStream in) throws XmlPullParserException, IOException {
//            try {
//                XmlPullParser parser = Xml.newPullParser();
//                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
//                parser.setInput(in, null);
//                parser.nextTag();
//                return readFeed(parser);
//            } finally {
//                in.close();
//            }
//        }
//
//    }
}
