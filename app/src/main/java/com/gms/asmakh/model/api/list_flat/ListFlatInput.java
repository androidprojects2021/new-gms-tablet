package com.gms.asmakh.model.api.list_flat;

import com.google.gson.annotations.SerializedName;

public class ListFlatInput {
    @SerializedName("building_id")
    private String building_id ="";

    public String getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(String building_id) {
        this.building_id = building_id;
    }
}
