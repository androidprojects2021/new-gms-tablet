package com.gms.asmakh.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class AppPreference {

    private Context context;
    private static final String KEY_LOGIN = "KEY_LOGIN";
    private static final String KEY_LOGOUT = "KEY_LOGOUT";
    private static final String ISLOGINSKIP = "ISLOGINSKIP";

    private static final String BUILDING_ID = "BUILDING_ID";
    private static final String BUILDING_NAME = "BUILDING_NAME";

    private static final String KEY_VERSION_NUMBER = "KEY_VERSION_NUMBER";
    private static final String LOCAL_LANG = "LOCAL_LANG";
    private static final String CITY_NAME = "CITY_NAME";
    private static final String CITY_ID = "CITY_ID";
    private static final String AREA_NAME = "AREA_NAME";
    private static final String AREA_ID = "AREA_ID";
    private static final String LATITUDE = "LATITUDE";
    private static final String LONGITUDE = "LONGITUDE";
    private static final String PRoPIc = "PRoPIc";
    private static final String PHONE = "PHONE";
    private static final String EMAIL = "EMAIL";
    private static final String TOKEN = "TOKEN";
    private static final String STREET = "STREET";
    private static final String BUILDING = "BUILDING";
    private static final String OUTLET = "OUTLET";
    private static final String OUTLET_ID = "OUTLET_ID";
    private static final String OUTLET_STATUS = "OUTLET_STATUS";


    public AppPreference(Context context) {

        this.context = context;
    }

    public void updateContext(Context context) {
        this.context = context;
    }

    private SharedPreferences getSharedPreferences() {
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MDSSALE", context.getApplicationContext().MODE_PRIVATE);
        return pref;
    }

    public boolean isLogin() {
        return getSharedPreferences().getBoolean(KEY_LOGIN, false);
    }

    public void setLoginStatus(boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(KEY_LOGIN, status);
        editor.apply();
        editor.commit();
    }

    public boolean loginSkip() {
        return getSharedPreferences().getBoolean(ISLOGINSKIP, false);
    }

    public void loginSkip(boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(ISLOGINSKIP, status);
        editor.apply();
        editor.commit();
    }


    public void setVersion(int version) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(KEY_VERSION_NUMBER, version);
        editor.apply();
        editor.commit();
    }


    public int getVersion() {
        return getSharedPreferences().getInt(KEY_VERSION_NUMBER, -1);
    }

    public void setLanguage(String version) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(LOCAL_LANG, version);
        editor.apply();
        editor.commit();
    }


    public String getLanguage() {
        return getSharedPreferences().getString(LOCAL_LANG, "");
    }


    public void setCityName(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(CITY_NAME, data);
        editor.apply();
        editor.commit();
    }


    public String getCityName() {
        return getSharedPreferences().getString(CITY_NAME, "");
    }


    public void setAreaName(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(AREA_NAME, data);
        editor.apply();
        editor.commit();
    }

    public String getLatitude() {
        return getSharedPreferences().getString(LATITUDE, "");
    }


    public void setLatitude(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(LATITUDE, data);
        editor.apply();
        editor.commit();
    }

    public String getLongitude() {
        return getSharedPreferences().getString(LONGITUDE, "");
    }


    public void setLongitude(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(LONGITUDE, data);
        editor.apply();
        editor.commit();
    }


    public String getAreaName() {
        return getSharedPreferences().getString(AREA_NAME, "");
    }


    public void setCityId(int data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(CITY_ID, data);
        editor.apply();
        editor.commit();
    }


    public int getCityId() {
        return getSharedPreferences().getInt(CITY_ID, -1);
    }


    public void setBuildingId(int data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(BUILDING_ID, data);
        editor.apply();
        editor.commit();
    }


    public int getBuildingId() {
        return getSharedPreferences().getInt(BUILDING_ID, -1);
    }


    public void setAreaId(int data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(AREA_ID, data);
        editor.apply();
        editor.commit();
    }


    public int getAreaId() {
        return getSharedPreferences().getInt(AREA_ID, -1);
    }


    public String getBuildingName() {
        return getSharedPreferences().getString(BUILDING_NAME, "");
    }


    public void setBuildingName(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(BUILDING_NAME, data);
        editor.apply();
        editor.commit();
    }


    public String getMobile() {
        return getSharedPreferences().getString(PHONE, "");
    }


    public void setMobile(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PHONE, data);
        editor.apply();
        editor.commit();
    }

    public String getProPic() {
        return getSharedPreferences().getString(PRoPIc, "");
    }


    public void setProPic(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PRoPIc, data);
        editor.apply();
        editor.commit();
    }


    public String getEmail() {
        return getSharedPreferences().getString(EMAIL, "");
    }


    public void setEmail(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(EMAIL, data);
        editor.apply();
        editor.commit();
    }


    public String getToken() {
        return getSharedPreferences().getString(TOKEN, "");
    }

    public void setToken(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(TOKEN, data);
        editor.apply();
        editor.commit();
    }

    public void setOutlet(String data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(OUTLET, data);
        editor.apply();
        editor.commit();
    }

    public String getOutlet() {
        return getSharedPreferences().getString(OUTLET, "");
    }

    public void setOutletId(int data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(OUTLET_ID, data);
        editor.apply();
        editor.commit();
    }

    public int getOutletId() {
        return getSharedPreferences().getInt(OUTLET_ID, -1);
    }

    public void setIsOutletChanged(boolean data) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(OUTLET_STATUS, data);
        editor.apply();
        editor.commit();
    }

    public boolean IsOutletChanged() {
        return getSharedPreferences().getBoolean(OUTLET_STATUS, false);
    }
}