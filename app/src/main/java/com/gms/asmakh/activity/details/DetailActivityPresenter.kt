package com.gms.guestmanagment.activity.details

import android.content.Context
import android.util.Log
import com.gms.asmakh.base.BasePresenter
import com.gms.asmakh.network.DatabaseCallback
import com.google.gson.JsonObject


class DetailActivityPresenter(detailActivityView: DetailActivityView?) : BasePresenter<DetailActivityView?>() {
    init {
        super.attachView(detailActivityView)
    }


    fun acceptedStatusDoorOpen(context: Context?) {
        addSubscribe(apiStores.getAcceptedStatus(), object : DatabaseCallback<JsonObject?>() {
            override fun onSuccess(model: JsonObject?) {
            }
            override fun onFailure(message: String) {}
            override fun onFinish() {}

        })
    }

}