package com.gms.guestmanagment.network

import com.gms.asmakh.model.api.checkregistration.building_login.BuildingLoginNetworkResponse
import com.gms.asmakh.model.api.checkregistration.building_login.InputBuildingLogin
import com.gms.asmakh.model.api.checkregistratn.CheckRegistrationNetworkResponse
import com.gms.asmakh.model.api.checkregistratn.InputCheckRegistration
import com.gms.asmakh.model.api.list_flat.FlatListNetworkResponse
import com.gms.asmakh.model.api.list_flat.ListFlatInput
import com.gms.asmakh.model.api.purpose_visit.PurposeNetworkResponse
import com.gms.asmakh.model.api.register.InputRegister
import com.gms.asmakh.model.api.register.RegisterNetworkResponse
import com.google.gson.JsonObject
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import rx.Observable

interface NetworkStores {

    //number type enter
    @POST("check_registration")
    fun check_registration(@Body inputLogin: InputCheckRegistration?): Observable<CheckRegistrationNetworkResponse?>?

//checkin
    @POST("building_login")
    fun get_login(@Body inputLogin: InputBuildingLogin?): Observable<BuildingLoginNetworkResponse?>?

    //submit
    @POST("user_register")
    fun user_register(@Body inputRegister: InputRegister?): Observable<RegisterNetworkResponse?>?

    @GET("purpose_of_visit")
    fun purpose_of_visit(): Observable<PurposeNetworkResponse?>?

    @GET("list_flats")
    fun list_flats(): Observable<FlatListNetworkResponse?>?


    @GET("http://192.168.0.25/on")
    fun getAcceptedStatus(): Observable<JsonObject?>?

    @POST("list_flats_by_building")
    fun list_flat_building(@Body listFlatInput: ListFlatInput?): Observable<FlatListNetworkResponse?>?




}