package com.gms.asmakh.activity.splash

import com.gms.asmakh.base.BasePresenter

class SplashActivityPresenter (splashView: SplashView?) : BasePresenter<SplashView?>() {

    init {
        super.attachView(splashView)
    }
}