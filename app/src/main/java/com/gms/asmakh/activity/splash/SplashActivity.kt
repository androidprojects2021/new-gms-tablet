package com.gms.asmakh.activity.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.gms.guestmanagment.activity.details.DetailActivity
import com.gms.asmakh.R
import com.gms.asmakh.base.MvpActivity

class SplashActivity  : MvpActivity<SplashActivityPresenter?>(), SplashView {

    var handler: Handler? = null

    override fun createPresenter(): SplashActivityPresenter {
        return SplashActivityPresenter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()
    }

    private fun callNextActivity() {
        handler!!.postDelayed({
                val intent = Intent(this@SplashActivity, DetailActivity::class.java)
                intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
                startActivity(intent)
                finish()

        }, DELAY_TIME.toLong())
    }

    private fun init() {}
    private fun onClick() {}
    override fun onResume() {
        super.onResume()
        if (handler == null) {
            handler = Handler()
        }
        callNextActivity()
    }

    override fun onPause() {
        super.onPause()
        handler!!.removeCallbacksAndMessages(null)
    }

    companion object {
        const val DELAY_TIME = 1000
        const val SPLASHACTIVITY = 12560
        const val SPLASH_ACTIVITY = "SplashActivity"
    }

}