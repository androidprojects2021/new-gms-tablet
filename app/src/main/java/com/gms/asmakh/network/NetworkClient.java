package com.gms.asmakh.network;


import androidx.multidex.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {
    private static Retrofit retrofit;

    public static Retrofit getRetrofit() {
        if (retrofit == null) {

            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(500, TimeUnit.SECONDS).readTimeout(500, TimeUnit.SECONDS).addInterceptor(interceptor).build();

                retrofit = new Retrofit.Builder()
                        .baseUrl(ConstantsURL.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(okHttpClient)
                        .build();
            } else {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


                OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(500, TimeUnit.SECONDS).readTimeout(500, TimeUnit.SECONDS).addInterceptor(interceptor).build();

                retrofit = new Retrofit.Builder()
                        /*.baseUrl(ConstantsURL.BASE_URL)*/
                        .baseUrl(ConstantsURL.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(okHttpClient)
                        .build();
            }
        }
        return retrofit;
    }
}