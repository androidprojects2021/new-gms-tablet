package com.gms.asmakh.model.api.fcm_payload;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Userdetail implements Serializable {

	@SerializedName("image")
	private String image;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("purpose")
	private String purpose;

	@SerializedName("flat_id")
	private String flatId;

	@SerializedName("name")
	private String name;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("accepted_status")
	private String acceptedStatus;

	@SerializedName("description")
	private String description;

	@SerializedName("location")
	private String location;

	@SerializedName("create_date")
	private String createDate;

	@SerializedName("status")
	private String status;

	public String getImage(){
		return image;
	}

	public String getUserId(){
		return userId;
	}

	public String getPurpose(){
		return purpose;
	}

	public String getFlatId(){
		return flatId;
	}

	public String getName(){
		return name;
	}

	public String getMobile(){
		return mobile;
	}

	public String getAcceptedStatus(){
		return acceptedStatus;
	}

	public String getDescription(){
		return description;
	}

	public String getLocation(){
		return location;
	}

	public String getCreateDate(){
		return createDate;
	}

	public String getStatus(){
		return status;
	}
}