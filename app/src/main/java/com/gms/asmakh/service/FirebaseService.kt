package com.gms.asmakh.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.gms.guestmanagment.activity.details.DetailActivity
import com.gms.asmakh.R
import com.gms.asmakh.model.api.fcm_payload.FcmPayout
import com.gms.asmakh.utils.NotificationUtils
import com.gms.asmakh.utils.ObjectFactory
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson

class FirebaseService : FirebaseMessagingService() {
    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.d("FCM_Token", s)
        ObjectFactory.getInstance(applicationContext).appPreferenceManager.token = s
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("onMessageReceived", remoteMessage.data.toString())
        Log.d("onMessageReceived", remoteMessage.notification!!.clickAction)

        val data = remoteMessage.data["payload"]
        val FcmPayout = Gson().fromJson(data, FcmPayout::class.java)

        var clickAction: String = ""

        clickAction = remoteMessage.notification?.clickAction ?: ""

        showNotification(remoteMessage.notification!!.title, remoteMessage.notification!!.body, FcmPayout, clickAction)


    }


    fun showNotification(title: String?, message: String?, datapayload: FcmPayout, clickaction: String) {
        if (!NotificationUtils.isAppIsInBackground(applicationContext)) {
            Log.d("FirebaseService", "if")

            val intent = Intent("com.gms.asmakh")
//            val intent = Intent("com.azinova.guestmanagment_fcm__payload")


            intent.putExtra("data", datapayload.userdetail)
            val localBroadcastReceiver = LocalBroadcastManager.getInstance(applicationContext)
            localBroadcastReceiver.sendBroadcast(intent)


        } else {
            message?.let {
                if (title != null) {
                    showNotification(title, it, clickaction)
                }
            }


        }
    }


    fun showNotification(title: String, message: String, clickAction: String) {
        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel = NotificationChannel("13",
                "CHANNEL_123",
                NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = "CHANNEL_DESCRIPTION"
            mNotificationManager.createNotificationChannel(channel)
        }
        val mBuilder = NotificationCompat.Builder(applicationContext, "YOUR_CHANNEL_ID")
            .setSmallIcon(R.mipmap.ic_launcher) // notification icon
            .setContentTitle(title) // title for notification
            .setContentText(message)// message for notification
            .setAutoCancel(true) // clear notification after click
        val intent = Intent(applicationContext, DetailActivity::class.java)
        intent.putExtra("fragmentName", DetailActivity.Pages.RECHECK_DETAILS)
        val pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        mBuilder.setContentIntent(pi)
        mNotificationManager.notify(0, mBuilder.build())
    }

}