package com.gms.asmakh.fragment.checkin

interface CheckInView {

    fun showLoader()

    fun hideLoader()

    fun onError(message: String?)

    fun onCheckinSuccess()
}