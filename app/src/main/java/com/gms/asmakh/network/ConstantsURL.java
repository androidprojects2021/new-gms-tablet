package com.gms.asmakh.network;

public class ConstantsURL {

public static final String BASE_URL = "http://demo.emaid.info:8083/guest-management/api/";//live url

//    public static final String BASE_URL = "http://demo.emaid.info:8083/guest-management-new-demo/api/";//demo url


    public static final String ACCEPTED = "1";
    public static final String REJECTED = "2";
    public static final String PENDING = "0";
}
