package com.gms.asmakh.model.api.qrResult;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Cte implements Serializable {

	@SerializedName("v6")
	private String v6;

	@SerializedName("v7")
	private String v7;

	@SerializedName("v1")
	private long v1;

	@SerializedName("v2")
	private int v2;

	@SerializedName("v3")
	private String v3;

	@SerializedName("v4")
	private String v4;

	@SerializedName("v5")
	private int v5;

	@SerializedName("v8")
	private String v8;

//	@SerializedName("v9")
//	private int v9;


	public String getV8() {
		return v8;
	}

//	public int getV9() {
//		return v9;
//	}

	public String getV6(){
		return v6;
	}

	public String getV7(){
		return v7;
	}

	public long getV1(){
		return v1;
	}

	public int getV2(){
		return v2;
	}

	public String getV3(){
		return v3;
	}

	public String getV4(){
		return v4;
	}

	public int getV5(){
		return v5;
	}
}