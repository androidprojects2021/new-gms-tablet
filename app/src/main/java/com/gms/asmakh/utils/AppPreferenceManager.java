package com.gms.asmakh.utils;

import android.content.Context;


public class AppPreferenceManager {


    Context context;

    public AppPreferenceManager(Context context) {

        this.context = context;
    }

    public void updateContext(Context context) {
        this.context = context;
    }


    public boolean getLoginStatus() {
        return ObjectFactory.getInstance(context).getAppPreference().isLogin();
    }

    public void setLoginStatus(boolean data) {
        ObjectFactory.getInstance(context).getAppPreference().setLoginStatus(data);
    }

//    public boolean getLogoutStatus() {
//        return ObjectFactory.getInstance(context).getAppPreference().isLogout();
//    }
//
//    public void setLogoutStatus(boolean data) {
//        ObjectFactory.getInstance(context).getAppPreference().setLogoutStatus(data);
//    }


    public boolean getLoginSkip() {
        return ObjectFactory.getInstance(context).getAppPreference().loginSkip();
    }

    public void setLoginSkip(boolean data) {
        ObjectFactory.getInstance(context).getAppPreference().loginSkip(data);
    }


    public int getVersionCode() {
        return ObjectFactory.getInstance(context).getAppPreference().getVersion();
    }


    public void setVersionCode(int data) {
        ObjectFactory.getInstance(context).getAppPreference().setVersion(data);
    }

    public String getLanguage() {
        return ObjectFactory.getInstance(context).getAppPreference().getLanguage();
    }


    public void setLanguage(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setLanguage(data);
    }

    public String getArea() {
        return ObjectFactory.getInstance(context).getAppPreference().getAreaName();
    }


    public void setArea(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setAreaName(data);
    }

    public String getCity() {
        return ObjectFactory.getInstance(context).getAppPreference().getCityName();
    }


    public void setCity(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setCityName(data);
    }

    public String getLatitude() {
        return ObjectFactory.getInstance(context).getAppPreference().getLatitude();
    }


    public void setlatitude(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setLatitude(data);
    }

    public String getLongitude() {
        return ObjectFactory.getInstance(context).getAppPreference().getLongitude();
    }


    public void setlongitude(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setLongitude(data);
    }


    public int getCityId() {
        return ObjectFactory.getInstance(context).getAppPreference().getCityId();
    }


    public void setCityId(int data) {
        ObjectFactory.getInstance(context).getAppPreference().setCityId(data);
    }

    public int getAreaId() {
        return ObjectFactory.getInstance(context).getAppPreference().getAreaId();
    }


    public void setAreaId(int data) {
        ObjectFactory.getInstance(context).getAppPreference().setAreaId(data);
    }

    public int getBuildingId() {
        return ObjectFactory.getInstance(context).getAppPreference().getBuildingId();
    }


    public void setBuildingId(int data) {
        ObjectFactory.getInstance(context).getAppPreference().setBuildingId(data);
    }

    public String getBuildingName() {
        return ObjectFactory.getInstance(context).getAppPreference().getBuildingName();
    }


    public void setBuildingName(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setBuildingName(data);
    }

    public String getMobile() {
        return ObjectFactory.getInstance(context).getAppPreference().getMobile();
    }


    public void setMobile(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setMobile(data);
    }

    public String getProImage() {
        return ObjectFactory.getInstance(context).getAppPreference().getProPic();
    }


    public void setProImage(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setProPic(data);
    }


    public String getEmail() {
        return ObjectFactory.getInstance(context).getAppPreference().getEmail();
    }


    public void setEmail(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setEmail(data);
    }


    public void setToken(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setToken(data);
    }

    public String getToken() {
        return ObjectFactory.getInstance(context).getAppPreference().getToken();
    }

    public void setOutlet(String data) {
        ObjectFactory.getInstance(context).getAppPreference().setOutlet(data);
    }

    public String getOutlet() {
        return ObjectFactory.getInstance(context).getAppPreference().getOutlet();
    }

    public void setOutletId(int data) {
        ObjectFactory.getInstance(context).getAppPreference().setOutletId(data);
    }

    public int getOutletId() {
        return ObjectFactory.getInstance(context).getAppPreference().getOutletId();
    }

    public void setIsOutletChanged(boolean data) {
        ObjectFactory.getInstance(context).getAppPreference().setIsOutletChanged(data);
    }

    public boolean isOutletChanged() {
        return ObjectFactory.getInstance(context).getAppPreference().IsOutletChanged();
    }


}
