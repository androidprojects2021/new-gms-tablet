package com.gms.asmakh.utils;
public class Constant {

    //Live Server URL

      public static final String WEB_URL = "http://34.238.104.190:8080/smartmanage/";

    //Local Server URL

//    public static final String WEB_URL="http://192.168.0.115:8080/smart_manage/";
//    public static final String IMAGE_URL="http://34.238.104.190:8080/Smart_ManageV2/images/";
    public static final String IMAGE_URL=WEB_URL+"images/";
    public static final String TEST_URL="http://whiteoval.000webhostapp.com/test.php";
    public static String FCM_REG_ID = "ah_firebase";
    public static final String TOPIC_GLOBAL = "global";
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String PUSH_NOTIFICATION = "pushNotification";

}
