package com.gms.asmakh.model.api.fcm_payload;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FcmPayout implements Serializable {

	@SerializedName("userdetail")
	private Userdetail userdetail;

	public Userdetail getUserdetail(){
		return userdetail;
	}
}