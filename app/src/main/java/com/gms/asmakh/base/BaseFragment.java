package com.gms.asmakh.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import rx.subscriptions.CompositeSubscription;


public class BaseFragment extends Fragment {
    public Activity activity;
    private CompositeSubscription compositeSubscription;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onUnsubscribe();
    }
    public void onUnsubscribe() {
        if (compositeSubscription != null) {
            compositeSubscription.unsubscribe();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onUnsubscribe();
    }


//    public void startActivity(DetailActivity.Pages page) {
//        Intent intent = new Intent(getContext(), DetailActivity.class);
//        intent.putExtra("fragmentName", page);
//        startActivity(intent);
//    }
//
//    public void startActivityForResuit(DetailActivity.Pages page, int requestCode) {
//        Intent intent = new Intent(getContext(), DetailActivity.class);
//        intent.putExtra("page", page);
//        startActivityForResult(intent,requestCode);
//
//    }
//        public void startActivityForResuit(DetailActivity.Pages page,int requestCode, Intent intent) {
//        intent.putExtra("page", page);
//        startActivityForResult(intent,requestCode);
//    }
//
//    public void startActivity(DetailActivity.Pages page, Intent intent) {
//        intent.putExtra("page", page);
//        startActivity(intent);
//    }

}
