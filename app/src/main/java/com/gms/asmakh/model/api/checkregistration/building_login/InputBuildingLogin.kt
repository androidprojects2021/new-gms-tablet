package com.gms.asmakh.model.api.checkregistration.building_login

import com.google.gson.annotations.SerializedName

class InputBuildingLogin {

    @SerializedName("code")
    var code: String? = null
}