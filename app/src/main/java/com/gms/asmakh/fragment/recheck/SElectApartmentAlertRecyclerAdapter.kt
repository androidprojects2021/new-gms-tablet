package com.gms.asmakh.fragment.recheck

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gms.asmakh.R
import com.gms.asmakh.model.api.list_flat.FlatsItem
import kotlinx.android.synthetic.main.custom_purpose_visit_view.view.*
import kotlinx.android.synthetic.main.custom_purpose_visit_view.view.textView_purpose
import kotlinx.android.synthetic.main.custoom_flat_item_view.view.*

class SElectApartmentAlertRecyclerAdapter (val context: Context, val flatsItem : List<FlatsItem> = ArrayList(), val clickedFlatNum : ClickItems) : RecyclerView.Adapter<SElectApartmentAlertRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SElectApartmentAlertRecyclerAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custoom_flat_item_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return flatsItem.size
    }

    override fun onBindViewHolder(holder: SElectApartmentAlertRecyclerAdapter.ViewHolder, position: Int) {
        holder.itemView.tvFlat.text = flatsItem[position].flatNum

        holder.itemView.setOnClickListener(View.OnClickListener {
            clickedFlatNum.clickedFlatNum(flatsItem[position].flatNum,flatsItem[position].flatId,flatsItem[position].buildingId)
        })

    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }


}