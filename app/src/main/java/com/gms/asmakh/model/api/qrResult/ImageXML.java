package com.gms.asmakh.model.api.qrResult;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImageXML implements Serializable {

	@SerializedName("cte")
	private Cte cte;

	@SerializedName("DS")
	private String dS;

	public Cte getCte(){
		return cte;
	}

	public String getDS(){
		return dS;
	}
}