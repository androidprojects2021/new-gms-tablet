package com.gms.asmakh.network;

import rx.Subscriber;


public abstract class DatabaseCallback<M> extends Subscriber<M> {

    private static final String TAG = DatabaseCallback.class.getName();

    public abstract void onSuccess(M model);

    public abstract void onFailure(String message);

    public abstract void onFinish();

    @Override
    public void onError(Throwable e) {

        onFinish();
    }

    @Override
    public void onNext(M model) {
        onSuccess(model);
    }

    @Override
    public void onCompleted() {
        onFinish();
    }
}
