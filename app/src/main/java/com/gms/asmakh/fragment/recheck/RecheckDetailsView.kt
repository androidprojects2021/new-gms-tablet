package com.gms.asmakh.fragment.recheck

import com.gms.asmakh.model.api.checkregistration.building_login.BuildingLoginNetworkResponse
import com.gms.asmakh.model.api.checkregistratn.CheckRegistrationNetworkResponse
import com.gms.asmakh.model.api.list_flat.FlatListNetworkResponse
import com.gms.asmakh.model.api.purpose_visit.PurposeNetworkResponse

interface RecheckDetailsView {

    fun showLoader()

    fun hideLoader()

    fun onError(message: String?)

    fun onUserRegistrationSuccess(message: String?)

    fun onCheckRegistrationSuccess(model: CheckRegistrationNetworkResponse)

    fun onPurposeSuccess(model:PurposeNetworkResponse)

    fun onFlatSuccess(model:FlatListNetworkResponse)

    fun RecheckDetailsSuccess(model: BuildingLoginNetworkResponse)

}