package com.gms.asmakh.fragment.success_fragment

import com.gms.asmakh.base.BasePresenter

class SuccessPresenter (successView  : SuccessView?) : BasePresenter<SuccessView?>(){

    init {
        super.attachView(successView)
    }
}