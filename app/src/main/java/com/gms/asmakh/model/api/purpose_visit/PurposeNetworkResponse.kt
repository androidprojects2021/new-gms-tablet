package com.gms.asmakh.model.api.purpose_visit

import com.google.gson.annotations.SerializedName

data class PurposeNetworkResponse(

		@field:SerializedName("details")
		val details: List<DetailsItem?>? = null,

		@field:SerializedName("status")
		val status: String? = null
)

data class DetailsItem(

		@field:SerializedName("p_v_id")
		val pVId: String? = null,

		@field:SerializedName("purpose")
		val purpose: String? = null,

		@field:SerializedName("status")
		val status: String? = null,

		var selected: Boolean = false


)
