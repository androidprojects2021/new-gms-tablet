package com.gms.asmakh.fragment.success_fragment

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.fxn.pix.Pix
import com.gms.asmakh.R
import com.gms.asmakh.base.MvpFragment
import com.gms.guestmanagment.activity.details.DetailActivity
import kotlinx.android.synthetic.main.recheck_details.*
import kotlinx.android.synthetic.main.success.*
import java.io.File

class SuccessFragment : MvpFragment<SuccessPresenter?>(), SuccessView {
    private var rootView: View? = null
    lateinit var new_request: TextView

    override fun createPresenter(): SuccessPresenter {
        return SuccessPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.success, container, false).also { rootView = it }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        onClick()
    }

    private fun init() {

        new_request = rootView!!.findViewById(R.id.new_request)
    }

    private fun onClick(){
        img_close_success.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            getActivity()?.finish()

        })

        new_request.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            getActivity()?.finish()

        })

        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                getActivity()?.finish()
                true
            } else false
        })

    }

    companion object {
        const val SUCCESS_FRAGMENT = "SuccessFragment"
    }


//     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//         when (requestCode) {
//             100 -> {
//                 if (resultCode == Activity.RESULT_OK) {
//
//
//                     view?.isFocusableInTouchMode = true
//                     view?.requestFocus()
//                     view?.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
//                         if (keyCode == KeyEvent.KEYCODE_BACK) {
//                             val intent = Intent(context, DetailActivity::class.java)
//                             intent.putExtra("fragmentName", DetailActivity.Pages.CHECK_IN)
//                             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                             intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
//                             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                             intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                             startActivity(intent)
//                             getActivity()?.finish()
//                             true
//                         } else false
//                     })
//
//                 }
//             }
//
//
//         }
//
//     }


//    view?.onKeyLongPress(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            //do something or nothing in your case
//            return true
//        }
//        return super.onKeyLongPress(keyCode, event);
//    }


}