package com.gms.asmakh.utils;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class ActivityUtils {

    /**
     * @param fragmentManager
     * @param fragment
     * @param frameId
     */
    public static void addFragment(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment, int frameId) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment, getTagForFragment(fragment));

        transaction.commit();
    }

    public static void showFragment(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment, int frameId, Fragment activeFragment) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.hide(activeFragment);
        transaction.show(fragment);

        transaction.commit();
    }

    public static String getTagForFragment(Fragment fragment) {
        return fragment.getClass().getSimpleName();
    }

    public static void addFragmentAddtoBackStack(@NonNull FragmentManager fragmentManager,
                                                 @NonNull Fragment fragment,
                                                 int frameId) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment, getTagForFragment(fragment));
        transaction.addToBackStack(getTagForFragment(fragment));
        transaction.commit();
    }

    public static void replaceFragmentAddtoBackStack(@NonNull FragmentManager fragmentManager,
                                                     @NonNull Fragment fragment,
                                                     int frameId) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment, getTagForFragment(fragment));
        transaction.addToBackStack(getTagForFragment(fragment));
        transaction.commit();
    }

    public static void replaceFragment(@NonNull FragmentManager fragmentManager,
                                       @NonNull Fragment fragment,
                                       int frameId) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment, getTagForFragment(fragment));
        transaction.commit();
    }

//    public static void replaceFragmentAnimation(@NonNull FragmentManager fragmentManager,
//                                                @NonNull Fragment fragment,
//                                                int frameId) {
//
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
//        transaction.replace(frameId, fragment, getTagForFragment(fragment));
//        transaction.commit();
//    }

    public static String getDate(Date date) {
        String dates = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd,yyyy  hh:mm a");
            dates = simpleDateFormat.format(date);

        } catch (Exception e) {
            dates = "";
        }

        return getTimeInFormatFromDateAndTime(dates);
    }


    public static String getDateOnly(Date date) {
        String dates = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd,yyyy");
            dates = simpleDateFormat.format(date);
        } catch (Exception e) {
            dates = "";
        }

        return getDateFormatFromDate(dates);
    }

    public static String getDateFormatFromDate(String dateandtime) {
        String localdate = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat format2 = new SimpleDateFormat("EEEE MMM dd, yyyy");
        format2.setTimeZone(TimeZone.getTimeZone("GMT+4"));
        Date date = null;
        try {
            date = format.parse(dateandtime);
            localdate = format2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return localdate;
        }
        return localdate;
    }

    public static String getTImeFormatFromDate(String dateandtime) {
        String localdate = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat format2 = new SimpleDateFormat("hh:mm a");
        format2.setTimeZone(TimeZone.getTimeZone("GMT+4"));
        Date date = null;
        try {
            date = format.parse(dateandtime);
            localdate = format2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return localdate;
        }
        return localdate;
    }

    public static String getTimeInFormatFromDateAndTime(String dateandtime) {
        String localdate = "";
        SimpleDateFormat format = new SimpleDateFormat("MMM dd,yyyy  hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat format2 = new SimpleDateFormat("MMM dd,yyyy  hh:mm a");
        format2.setTimeZone(TimeZone.getTimeZone("GMT+4"));
        Date date = null;
        try {
            date = format.parse(dateandtime);
            localdate = format2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return localdate;
        }
        return localdate;
    }

    public static String getTimeInFormatFromDateAndTime2(String dateandtime) {
        String localdate = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat format2 = new SimpleDateFormat("MMM dd,yyyy hh:mm a");
        format2.setTimeZone(TimeZone.getTimeZone("GMT+4"));
        Date date = null;
        try {
            date = format.parse(dateandtime);
            localdate = format2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return localdate;
        }
        return localdate;
    }


    public static Date getDateLocal(String dateandtime) {
        String localdate = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(dateandtime);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getTimeDifference(String startTime, String endTime) {

        if (TextUtils.isEmpty(startTime)) {
            return "";
        }
        if (TextUtils.isEmpty(endTime)) {
            return "";
        }
        long mills = Long.parseLong(endTime) - Long.parseLong(startTime);

        int hours = (int) (mills / (1000 * 60 * 60));
        int mins = (int) (mills / (1000 * 60)) % 60;
        long Secs = (int) (mills / 1000) % 60;

        String diff = hours + ":" + mins + ":" + Secs; // updated value every1 second
        return diff;
    }


//    public static long showNetworkError(final Context context, View rootView, long lastNetworkErrorShownTime) {
//
//        if (rootView != null) {
//            long currentTimeMillis = System.currentTimeMillis();
//            if ((currentTimeMillis - lastNetworkErrorShownTime) > 5000) {
//                Snackbar snackbar = Snackbar.make(rootView, "No active network connection", Snackbar.LENGTH_LONG);
//                snackbar.setAction(R.string.settings, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
//                        context.startActivity(intent);
//                    }
//                });
//                snackbar.show();
//                return currentTimeMillis;
//            }
//        }
//        return 0;
//    }


    public static void showErrorToast( String message,View rootView) {

        if (rootView != null) {
            Snackbar snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            TextView v = (TextView) snackbar.getView().findViewById(android.R.id.message);
            if (v != null) v.setGravity(Gravity.CENTER);

            snackbar.show();
        }
//        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
//        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
//        if (v != null) v.setGravity(Gravity.CENTER);
//        toast.show();
    }

    public static String getDateandTime(String unformated_date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        String returnDate;
        String[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};
        try {
            Date date = simpleDateFormat.parse(unformated_date);
            calendar.setTime(date);
          /*  int AM_PM = calendar.get(Calendar.AM_PM);
            String am;
            if (AM_PM == 0){
               am = "AM" ;
            }else {
                am = "PM";
            }*/
            calendar.setTimeZone(TimeZone.getDefault());

            returnDate = monthNames[calendar.get(Calendar.MONTH)] + " " +calendar.get(Calendar.DAY_OF_MONTH) + ", "  + calendar.get(Calendar.YEAR) + " at "+calendar.get(Calendar.HOUR)+":"+ calendar.get(Calendar.MINUTE) + " "+"am";
        } catch (ParseException e) {
            e.printStackTrace();
            returnDate = "";
        }
        return returnDate;
    }

}
